%undefine _hardened_build

Name:          xorg-x11-drv-qxl
Version:       0.1.6
Release:       2
Summary:       Qxl video driver for the X Window System
License:       MIT
URL:           http://www.x.org
Source0:       http://xorg.freedesktop.org/releases/individual/driver/xf86-video-qxl-%{version}.tar.xz
Patch0001:     0001-worst-hack-of-all-time-to-qxl-driver.patch
Patch0002:     0002-Xspice-Adjust-shebang-to-explicitly-mention-python3.patch
Patch0003:     0003-fix-CARD.patch

BuildRequires: pkgconfig git-core xorg-x11-server-devel >= 1.1.0-1 spice-protocol >= 0.12.1
BuildRequires: libdrm-devel >= 2.4.46-1 spice-server-devel >= 0.8.0 glib2-devel
BuildRequires: libtool libudev-devel libXfont2-devel libXext-devel
Requires:      Xorg %(xserver-sdk-abi-requires ansic) %(xserver-sdk-abi-requires videodrv)

%description
The xorg-x11-drv-qxl packages provide an X11 video driver for the QEMU QXL video accelerator.

%package -n    xorg-x11-server-Xspice
Summary:       Server for X or spice clients
Requires:      Xorg %(xserver-sdk-abi-requires ansic) %(xserver-sdk-abi-requires videodrv)
Requires:      xorg-x11-server-Xorg

%description -n xorg-x11-server-Xspice
Xspice creates a virtual X server that you can then connect to remotely using any Spice client.

%prep
%autosetup -n xf86-video-qxl-%{version} -p1

%build
autoreconf -ivf
%configure --enable-xspice
%make_build

%install
%make_install
install -Dp -m644 examples/spiceqxl.xorg.conf.example $RPM_BUILD_ROOT%{_sysconfdir}/X11/spiceqxl.xorg.conf
%delete_la

%files
%doc COPYING README.md
%(pkg-config xorg-server --variable=moduledir)/drivers/qxl_drv.so
%exclude /usr/share/doc/xf86-video-qxl/spiceqxl.xorg.conf.example

%files -n xorg-x11-server-Xspice
%doc COPYING README.xspice README.md examples/spiceqxl.xorg.conf.example
%config(noreplace) %{_sysconfdir}/X11/spiceqxl.xorg.conf
%{_bindir}/Xspice
%(pkg-config xorg-server --variable=moduledir)/drivers/spiceqxl_drv.so

%changelog
* Fri Mar 07 2025 mahailiang <mahailiang@uniontech.com> - 0.1.6-2
- fix sw_64 build error

* Sat Jan 28 2023 Chenxi Mao <chenxi.mao@suse.com> - 0.1.6-1
- Upgrade to 0.1.6

* Tue Oct 25 2022 wangkerong <wangkerong@h-partners.com> - 0.1.5-15
- rebuild for next release

* Sat Jan 8 2022 yangcheng <yangcheng87@huawei.com> - 0.1.5-14
- fix build error with xserver

* Thu Aug 6 2020 xiaoweiwei <xiaoweiwei5@huawei.com> - 0.1.5-13
- Add compilation dependency to solve compilation failture

* Thu Nov 28 2019 shijian <shijian16@huawei.com> - 0.1.5-11
- Package init
